package com.nilhcem.chemion.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.nilhcem.chemion.R
import com.nilhcem.chemion.ui.message.MessageFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        showMessageScreen()
    }

    private fun showMessageScreen() {
        val fragment = MessageFragment.create()
        supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragment_container, fragment)
                .commit()
    }
}
