package com.nilhcem.chemion.ui.message

import android.content.Context
import androidx.lifecycle.ViewModel
import com.nilhcem.chemion.core.android.logger.Log
import com.nilhcem.chemion.data.bluetooth.GattClient
import com.nilhcem.chemion.data.bluetooth.ScanHelper
import com.nilhcem.chemion.data.chemion.Chemion
import com.nilhcem.chemion.data.chemion.DataToByteArraysConverter
import com.nilhcem.chemion.data.chemion.textconverter.TextConverter

class MessageViewModel(private val context: Context, private val textConverter: TextConverter, private val dataToByteArraysConverter: DataToByteArraysConverter, private val scanHelper: ScanHelper, private val gattClient: GattClient) : ViewModel() {

    private var isConnected: Boolean = false

    fun sendMessage(message: String) {
        Log.d { "Sending message: $message" }

        val messageDataArray = textConverter.convert(message, Chemion.Color.HIGH)
        val data = copyDataArray(messageDataArray)
        val byteData = dataToByteArraysConverter.convert(data)
        sendBytes(context, byteData)
    }

    override fun onCleared() {
        super.onCleared()
        isConnected = false
        scanHelper.stopLeScan()
        gattClient.stopClient()
    }

    private fun sendBytes(context: Context, byteData: List<ByteArray>) {
        if (isConnected) {
            sendData(byteData)
        } else {
            scanHelper.startLeScan { device ->
                if (device == null) {
                    Log.e { "Scan could not find any device" }
                } else {
                    Log.e { "Device found: $device" }

                    if (device.address.startsWith("EB:DE:48:D6")) {
                        gattClient.startClient(context, device.address) { onConnected ->
                            isConnected = onConnected

                            if (onConnected) {
                                sendData(byteData)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun sendData(byteData: List<ByteArray>) {
        gattClient.writeDataStart(byteData) {
            Log.i { "Data sent" }
        }
    }

    private fun copyDataArray(messageDataArray: Array<Array<Chemion.Color>>): Array<Array<Chemion.Color>> {
        val dataArray = Array(Chemion.LEDS_COL_COUNT) { Array(Chemion.LEDS_ROW_COUNT) { Chemion.Color.OFF } }

        val xStart = if (messageDataArray.size >= dataArray.size) 0 else {
            (0.5f * (dataArray.size - messageDataArray.size)).toInt()
        }

        val yStart = if (messageDataArray.isEmpty()) 0 else {
            (0.5 * (dataArray[0].size - messageDataArray[0].size)).toInt()
        }

        for (x in 0 until Math.min(dataArray.size, messageDataArray.size)) {
            for (y in 0 until Math.min(dataArray[x].size, messageDataArray[x].size)) {
                dataArray[xStart + x][yStart + y] = messageDataArray[x][y]
            }
        }

        return dataArray
    }
}
