package com.nilhcem.chemion.ui.message

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.nilhcem.chemion.R
import com.nilhcem.chemion.core.android.ext.showKeyboard
import com.nilhcem.chemion.core.android.logger.Log
import kotlinx.android.synthetic.main.message_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MessageFragment : Fragment() {

    companion object {
        private const val REQUEST_ENABLE_BT = 1
        private const val REQUEST_PERMISSION_LOCATION = 1

        fun create() = MessageFragment()
    }

    private val viewModel: MessageViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.message_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepareForScan()

        text_to_send.setText("COOL!")

        text_to_send.requestFocus()
        text_to_send.showKeyboard()

        send_button.setOnClickListener {
            viewModel.sendMessage(text_to_send.text.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(context, R.string.enable_bluetooth, Toast.LENGTH_LONG).show()
            exitApp()
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSION_LOCATION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d { "Location permission accepted" }
            } else {
                Toast.makeText(context, R.string.grant_location_permission, Toast.LENGTH_SHORT).show()
                exitApp()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun prepareForScan() {
        if (isBleSupported()) {
            // Ensures Bluetooth is enabled on the device
            val btManager = context!!.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            val btAdapter = btManager.adapter
            if (btAdapter.isEnabled) {
                // Prompt for runtime permission
                if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Log.i { "Coarse permission granted" }
                } else {
                    ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_PERMISSION_LOCATION)
                }
            } else {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
            }
        } else {
            Toast.makeText(context, "BLE is not supported", Toast.LENGTH_LONG).show()
            exitApp()
        }
    }

    private fun isBleSupported() = context!!.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)

    private fun exitApp() = activity!!.finish()
}
