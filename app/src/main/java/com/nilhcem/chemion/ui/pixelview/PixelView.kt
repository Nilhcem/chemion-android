package com.nilhcem.chemion.ui.pixelview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.nilhcem.chemion.data.chemion.Chemion

class PixelView : View, View.OnTouchListener {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private val grid = Array(Chemion.LEDS_ROW_COUNT) { Array(Chemion.LEDS_COL_COUNT) { Chemion.Color.OFF } }
    private val rect = Rect()

    private lateinit var paint: Paint
    private /*lateinit*/ var pixelSize = 0

    private val backgroundColor = 0xffff00ff.toInt()
    private var selectedColor = Chemion.Color.OFF

    private fun init(context: Context) {
        setOnTouchListener(this)

        paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = Color.CYAN
            style = Paint.Style.FILL
        }

        grid[3][4] = Chemion.Color.HIGH
        grid[3][5] = Chemion.Color.MEDIUM
        grid[3][6] = Chemion.Color.LOW
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val width = getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)

        pixelSize = Math.round(width.toFloat() / Chemion.LEDS_COL_COUNT)

        val computedHeight = pixelSize * Chemion.LEDS_ROW_COUNT
        setMeasuredDimension(width, computedHeight)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawColor(backgroundColor)

        for (row in 0 until Chemion.LEDS_ROW_COUNT) {
            for (col in 0 until Chemion.LEDS_COL_COUNT) {
                paint.color = grid[row][col].androidColor

                rect.top = row * pixelSize
                rect.left = col * pixelSize
                rect.right = rect.left + pixelSize
                rect.bottom = rect.top + pixelSize

                canvas.drawRect(rect, paint)
            }
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
