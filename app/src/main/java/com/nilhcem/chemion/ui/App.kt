package com.nilhcem.chemion.ui

import android.app.Application
import com.nilhcem.chemion.BuildConfig
import com.nilhcem.chemion.core.di.Di
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Di.init(this)
        initLogger()
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
