package com.nilhcem.chemion.core.di

import android.content.Context
import com.nilhcem.chemion.core.di.modules.appModule
import org.koin.standalone.StandAloneContext.startKoin

object Di {

    fun init(context: Context) {
        startKoin(listOf(appModule(context)))
    }
}
