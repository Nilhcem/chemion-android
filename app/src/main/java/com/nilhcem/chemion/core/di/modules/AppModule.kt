package com.nilhcem.chemion.core.di.modules

import android.content.Context
import com.nilhcem.chemion.data.bluetooth.GattClient
import com.nilhcem.chemion.data.bluetooth.ScanHelper
import com.nilhcem.chemion.data.chemion.DataToByteArraysConverter
import com.nilhcem.chemion.data.chemion.textconverter.B64TextConverter
import com.nilhcem.chemion.data.chemion.textconverter.BitmapTextConverter
import com.nilhcem.chemion.data.chemion.textconverter.ChemionTextConverter
import com.nilhcem.chemion.data.chemion.textconverter.TextConverter
import com.nilhcem.chemion.ui.message.MessageViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

fun appModule(context: Context) = module {
    single { context }
    single { DataToByteArraysConverter() }

    // Use one of those implementations
    single<TextConverter> { ChemionTextConverter() }
//    single<TextConverter> { B64TextConverter(get()) }
//    single<TextConverter> { BitmapTextConverter(get()) }

    single { ScanHelper() }
    single { GattClient() }

    viewModel { MessageViewModel(get(), get(), get(), get(), get()) }
}
