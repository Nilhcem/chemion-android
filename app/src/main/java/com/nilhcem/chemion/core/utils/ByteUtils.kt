package com.nilhcem.chemion.core.utils

import java.util.*

object ByteUtils {

    fun byteArrayToHexString(byteArray: ByteArray): String {
        return byteArray.joinToString(separator = "") { byteToHexString(it) }
    }

    private fun byteToHexString(byte: Byte): String {
        return String.format(Locale.US, "%02X", byte)
    }
}
