package com.nilhcem.chemion.data.bluetooth

import android.bluetooth.BluetoothDevice
import android.os.Handler
import android.os.ParcelUuid
import com.nilhcem.chemion.core.android.logger.Log
import com.nilhcem.chemion.data.bluetooth.Constants.SERVICE_UART_UUID
import no.nordicsemi.android.support.v18.scanner.*

class ScanHelper {

    companion object {
        private const val SCAN_TIMEOUT_MS = 10_000L
    }

    private var isScanning = false
    private var onDeviceFoundCallback: ((BluetoothDevice?) -> Unit)? = null

    private val scanner by lazy { BluetoothLeScannerCompat.getScanner() }
    private val stopScanHandler = Handler()
    private val stopScanRunnable = Runnable {
        Log.i { "No devices found" }
        stopLeScan()
        onDeviceFoundCallback?.invoke(null)
    }

    private val scanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            Log.i { "onScanResult: ${result.device.address}" }
            stopLeScan()
            onDeviceFoundCallback?.invoke(result.device)
        }

        override fun onScanFailed(errorCode: Int) {
            Log.w { "Scan failed: $errorCode" }
            stopLeScan()
            onDeviceFoundCallback?.invoke(null)
        }
    }

    fun startLeScan(onDeviceFoundCallback: ((BluetoothDevice?) -> Unit)) {
        this.onDeviceFoundCallback = onDeviceFoundCallback
        isScanning = true

        val filters = listOf(ScanFilter.Builder()
                .setServiceUuid(ParcelUuid(SERVICE_UART_UUID))
                .build())

        val settings = ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build()

        scanner.startScan(filters, settings, scanCallback)

        // Stops scanning after a pre-defined scan period.
        stopScanHandler.postDelayed(stopScanRunnable, SCAN_TIMEOUT_MS)
    }

    fun stopLeScan() {
        if (isScanning) {
            isScanning = false

            scanner.stopScan(scanCallback)
            stopScanHandler.removeCallbacks(stopScanRunnable)
        }
    }
}
