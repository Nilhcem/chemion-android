package com.nilhcem.chemion.data.chemion

import androidx.annotation.ColorInt

object Chemion {

    const val LEDS_ROW_COUNT = 9
    const val LEDS_COL_COUNT = 24

    enum class Color(@ColorInt val androidColor: Int, val chemionColor: Int) {
        LOW(0x55ffffff, 0b01),
        MEDIUM(0xaaffffff.toInt(), 0b10),
        HIGH(0xffffffff.toInt(), 0b11),
        OFF(0x00000000, 0b00)
    }
}
