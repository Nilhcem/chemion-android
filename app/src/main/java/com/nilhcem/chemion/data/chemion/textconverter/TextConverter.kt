package com.nilhcem.chemion.data.chemion.textconverter

import com.nilhcem.chemion.data.chemion.Chemion

interface TextConverter {

    fun convert(message: String, color: Chemion.Color): Array<Array<Chemion.Color>>
}
