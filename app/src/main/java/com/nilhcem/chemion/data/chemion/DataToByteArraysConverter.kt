package com.nilhcem.chemion.data.chemion

import kotlin.experimental.xor

class DataToByteArraysConverter {

    companion object {
        private const val PACKET_BYTE_SIZE = 20
    }

    fun convert(data: Array<Array<Chemion.Color>>): List<ByteArray> {
        val payload = toPayload(data)
        val encodedMessage = encodeMessage(payload)

        return encodedMessage
                .asSequence()
                .chunked(PACKET_BYTE_SIZE)
                .map { it.toByteArray() }
                .toList()
    }

    private fun toPayload(data: Array<Array<Chemion.Color>>): List<Byte> {
        val payload = mutableListOf<Byte>(0x01, 0x00, 0x06)
        payload.addAll(dataArrayToByteList(data))

        return payload
    }

    private fun dataArrayToByteList(data: Array<Array<Chemion.Color>>): List<Byte> {
        val byteList = mutableListOf<Byte>()
        var curByte = 0

        for (y in 0 until Chemion.LEDS_ROW_COUNT) {
            for (x in 0 until Chemion.LEDS_COL_COUNT) {
                val chemionValue = data[x][y].chemionColor
                val shlValue = 6 - (x % 4) * 2
                curByte = curByte or (chemionValue shl shlValue)

                if ((x + 1) % 4 == 0) {
                    byteList.add(curByte.toByte())
                    curByte = 0x00
                }
            }
        }

        return byteList
    }

    private fun encodeMessage(payload: List<Byte>): List<Byte> {
        val message = mutableListOf<Byte>()

        // Header
        message.add(0xfa.toByte())
        message.add(0x03)

        // Payload size
        message.add((payload.size / 0xff).toByte())
        message.add((payload.size % 0xff).toByte())

        // Payload
        message.addAll(payload)

        // Payload checksum
        message.add(payload.reduce { acc, byte -> acc xor byte })

        // Footer
        message.add(0x55)
        message.add(0xa9.toByte())

        return message
    }
}
