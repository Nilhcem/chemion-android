package com.nilhcem.chemion.data.chemion.textconverter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import com.nilhcem.chemion.R
import com.nilhcem.chemion.data.chemion.Chemion

class B64TextConverter(context: Context) : TextConverter {

    companion object {
        private const val CHARACTERS_PER_LINE = 32
        private const val FONT_START = ' '
        private const val FONT_END = '~'

        private const val CHAR_WIDTH = 4
        private const val CHAR_HEIGHT = 8
        private const val SPACE_BETWEEN_CHARS = 1
    }

    private val charsTable: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.b64)

    override fun convert(message: String, color: Chemion.Color): Array<Array<Chemion.Color>> {
        val messageLength = message.length
        val width = Math.max(0, messageLength * CHAR_WIDTH + (messageLength - 1) * SPACE_BETWEEN_CHARS)
        val height = CHAR_HEIGHT
        val grid = Array(width) { Array(height) { Chemion.Color.OFF } }

        for (i in 0 until messageLength) {
            val c = message[i]
            val left = Math.round(i.toFloat() * CHAR_WIDTH + if (i == 0) 0 else i * SPACE_BETWEEN_CHARS)

            val (coordX, coordY) = getCoordsForChar(c)
            for (x in 0 until CHAR_WIDTH) {
                for (y in 0 until CHAR_HEIGHT) {
                    val pixel = charsTable.getPixel(coordX + x, coordY + y)
                    grid[x + left][y] = if (pixel == Color.WHITE) Chemion.Color.OFF else color
                }
            }
        }
        return grid
    }

    private fun getCoordsForChar(c: Char): Pair<Int, Int> {
        if (c < FONT_START || c > FONT_END) {
            return getCoordsForChar('?')
        }

        return Pair(
                (c - FONT_START) % CHARACTERS_PER_LINE * CHAR_WIDTH,
                (c - FONT_START) / CHARACTERS_PER_LINE * CHAR_HEIGHT
        )
    }
}
