package com.nilhcem.chemion.data.chemion.textconverter

import android.content.Context
import android.graphics.*
import com.nilhcem.chemion.R
import com.nilhcem.chemion.data.chemion.Chemion

class BitmapTextConverter(context: Context) : TextConverter {

    companion object {
        private const val CHARS = " +-*/!\"#$><0123456789.=)(ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz?,;:|@%[&_']\\~"
        private const val CHAR_WIDTH = 5
        private const val CHAR_HEIGHT = 8
        private const val SPACE_BETWEEN_CHARS = 1
    }

    private val charsTable: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.font)
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    override fun convert(message: String, color: Chemion.Color): Array<Array<Chemion.Color>> {
        val messageLength = message.length
        val width = messageLength * CHAR_WIDTH + (messageLength - 1) * SPACE_BETWEEN_CHARS
        val height = CHAR_HEIGHT
        val grid = Array(width) { Array(height) { Chemion.Color.OFF } }

        for (i in 0 until messageLength) {
            val c = message[i]
            val b = getBitmapFor(c)
            val left = Math.round(i.toFloat() * CHAR_WIDTH + if (i == 0) 0 else i * SPACE_BETWEEN_CHARS)

            for (x in 0 until b.width) {
                for (y in 0 until b.height) {
                    val pixel = b.getPixel(x, y)
                    grid[x + left][y] = if (pixel == Color.TRANSPARENT) Chemion.Color.OFF else color
                }
            }
        }
        return grid
    }

    private fun getBitmapFor(ch: Char): Bitmap {
        val index = CHARS.indexOf(ch)

        val bitmap = Bitmap.createBitmap(CHAR_WIDTH, CHAR_HEIGHT, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.drawBitmap(charsTable, Rect(index * CHAR_WIDTH, 0, (index + 1) * CHAR_WIDTH, CHAR_HEIGHT), Rect(0, 0, CHAR_WIDTH, CHAR_HEIGHT), paint)
        return bitmap
    }
}
