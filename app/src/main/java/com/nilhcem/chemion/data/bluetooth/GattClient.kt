package com.nilhcem.chemion.data.bluetooth

import android.bluetooth.*
import android.content.Context
import com.nilhcem.chemion.core.android.logger.Log
import com.nilhcem.chemion.core.utils.ByteUtils
import com.nilhcem.chemion.data.bluetooth.Constants.CHARACTERISTIC_TX_UUID
import com.nilhcem.chemion.data.bluetooth.Constants.SERVICE_UART_UUID
import java.util.*

class GattClient {

    private var bluetoothManager: BluetoothManager? = null
    private var bluetoothAdapter: BluetoothAdapter? = null
    private var bluetoothGatt: BluetoothGatt? = null
    private var onConnectedListener: ((Boolean) -> Unit)? = null
    private var onFinishWritingDataListener: (() -> Unit)? = null

    private val messagesToSend = LinkedList<ByteArray>()

    private val gattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i { "Connected to GATT client. Attempting to start service discovery" }
                gatt.discoverServices()
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i { "Disconnected from GATT client" }
                stopClient()
                onConnectedListener?.invoke(false)
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                onConnectedListener?.invoke(true)
            } else {
                Log.w { "onServicesDiscovered received: $status" }
                stopClient()
                onConnectedListener?.invoke(false)
            }
        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            Log.i { "onCharacteristicWrite" }
            Thread.sleep(100)
            writeNextData()
        }
    }

    fun writeDataStart(byteData: List<ByteArray>, onFinishWritingDataListener: () -> Unit) {
        this.onFinishWritingDataListener = onFinishWritingDataListener
        messagesToSend.addAll(byteData)
        writeNextData()
    }

    fun writeNextData() {
        if (messagesToSend.isEmpty()) {
            onFinishWritingDataListener?.invoke()
        } else {
            val data = messagesToSend.pop()
            Log.e { "Writing: ${ByteUtils.byteArrayToHexString(data)}" }

            val characteristic = bluetoothGatt?.getService(SERVICE_UART_UUID)?.getCharacteristic(CHARACTERISTIC_TX_UUID)
            characteristic?.value = data
            bluetoothGatt?.writeCharacteristic(characteristic)
        }
    }

    fun startClient(context: Context, deviceAddress: String, onConnectedListener: (Boolean) -> Unit) {
        this.onConnectedListener = onConnectedListener
        bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager?.adapter

        val bluetoothDevice = bluetoothAdapter?.getRemoteDevice(deviceAddress)
        bluetoothGatt = bluetoothDevice?.connectGatt(context, false, gattCallback)

        if (bluetoothGatt == null) {
            Log.w { "Unable to create GATT client" }
            return
        }
    }

    fun stopClient() {
        if (bluetoothGatt != null) {
            bluetoothGatt?.close()
            bluetoothGatt = null
        }

        if (bluetoothAdapter != null) {
            bluetoothAdapter = null
        }
    }
}
