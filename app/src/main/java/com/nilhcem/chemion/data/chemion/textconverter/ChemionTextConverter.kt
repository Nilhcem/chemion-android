package com.nilhcem.chemion.data.chemion.textconverter

import com.nilhcem.chemion.data.chemion.Chemion

class ChemionTextConverter : TextConverter {

    companion object {
        private const val CHAR_HEIGHT = 9
        private const val SPACE_BETWEEN_CHARS = 1

        private val CHARSETS = mapOf(
                ' ' to listOf(4, 0b0000),
                '!' to listOf(1, 0b0, 0b1, 0b1, 0b1, 0b1, 0b0, 0b1, 0b1),
                '"' to listOf(6, 0b011011, 0b010010, 0b100100),
                '#' to listOf(8, 0b00000000, 0b00010010, 0b00010010, 0b01111111, 0b00100100, 0b11111110, 0b01001000, 0b01001000),
                '$' to listOf(5, 0b00100, 0b01111, 0b10101, 0b10100, 0b01110, 0b00101, 0b10101, 0b11110, 0b00100),
                '%' to listOf(9, 0b000000000, 0b011000010, 0b100100100, 0b100101000, 0b011010110, 0b000101001, 0b001001001, 0b010000110),
                '&' to listOf(6, 0b000000, 0b011000, 0b100100, 0b100100, 0b011000, 0b100101, 0b100010, 0b011101),
                '\'' to listOf(3, 0b011, 0b010, 0b100),
                '(' to listOf(2, 0b01, 0b01, 0b10, 0b10, 0b10, 0b10, 0b10, 0b01, 0b01),
                ')' to listOf(2, 0b10, 0b10, 0b01, 0b01, 0b01, 0b01, 0b01, 0b10, 0b10),
                '*' to listOf(5, 0b00000, 0b00100, 0b10101, 0b01110, 0b01110, 0b10101, 0b00100),
                '+' to listOf(3, 0b000, 0b000, 0b000, 0b010, 0b111, 0b010),
                ',' to listOf(2, 0b00, 0b00, 0b00, 0b00, 0b00, 0b00, 0b11, 0b01, 0b10),
                '-' to listOf(5, 0b00000, 0b00000, 0b00000, 0b00000, 0b11111),
                '.' to listOf(2, 0b00, 0b00, 0b00, 0b00, 0b00, 0b00, 0b00, 0b11, 0b11),
                '/' to listOf(7, 0b0000000, 0b0000001, 0b0000010, 0b0000100, 0b0001000, 0b0010000, 0b0100000, 0b1000000),
                '0' to listOf(4, 0b0000, 0b0110, 0b1101, 0b1101, 0b1101, 0b1101, 0b1101, 0b0110),
                '1' to listOf(4, 0b0000, 0b0110, 0b1110, 0b0110, 0b0110, 0b0110, 0b0110, 0b1111),
                '2' to listOf(4, 0b0000, 0b0110, 0b1011, 0b0011, 0b0110, 0b1100, 0b1100, 0b1111),
                '3' to listOf(4, 0b0000, 0b0110, 0b1011, 0b0011, 0b0110, 0b0011, 0b1011, 0b0110),
                '4' to listOf(4, 0b0000, 0b0010, 0b0110, 0b1100, 0b1010, 0b1111, 0b0010, 0b0010),
                '5' to listOf(4, 0b0000, 0b1111, 0b1100, 0b1100, 0b1110, 0b0011, 0b1011, 0b0110),
                '6' to listOf(4, 0b0000, 0b0110, 0b1101, 0b1100, 0b1111, 0b1101, 0b1101, 0b0110),
                '7' to listOf(4, 0b0000, 0b1111, 0b1011, 0b0011, 0b0110, 0b0110, 0b1100, 0b1100),
                '8' to listOf(4, 0b0000, 0b0110, 0b1101, 0b1101, 0b1111, 0b1101, 0b1101, 0b0110),
                '9' to listOf(4, 0b0000, 0b0110, 0b1101, 0b1101, 0b1111, 0b0001, 0b1101, 0b0110),
                ':' to listOf(1, 0b0, 0b0, 0b1, 0b1, 0b0, 0b1, 0b1),
                ';' to listOf(2, 0b00, 0b00, 0b01, 0b01, 0b00, 0b01, 0b01, 0b10),
                '<' to listOf(7, 0b0000000, 0b0000011, 0b0001100, 0b0110000, 0b1000000, 0b0110000, 0b0001100, 0b0000011),
                '=' to listOf(7, 0b0000000, 0b0000000, 0b0000000, 0b1111111, 0b0000000, 0b1111111),
                '>' to listOf(7, 0b0000000, 0b1100000, 0b0011000, 0b0000110, 0b0000001, 0b0000110, 0b0011000, 0b1100000),
                '?' to listOf(4, 0b0000, 0b0110, 0b1001, 0b1001, 0b0010, 0b0010, 0b0000, 0b0010),
                '@' to listOf(7, 0b0000000, 0b0011110, 0b0100001, 0b1001101, 0b1010101, 0b1011110, 0b1000001, 0b0111110),
                'A' to listOf(4, 0b0000, 0b0110, 0b1111, 0b1101, 0b1111, 0b1101, 0b1101, 0b1101),
                'B' to listOf(4, 0b0000, 0b1110, 0b1101, 0b1101, 0b1110, 0b1101, 0b1101, 0b1110),
                'C' to listOf(4, 0b0000, 0b0111, 0b1100, 0b1100, 0b1100, 0b1100, 0b1100, 0b0111),
                'D' to listOf(4, 0b0000, 0b1110, 0b1101, 0b1101, 0b1101, 0b1101, 0b1101, 0b1110),
                'E' to listOf(4, 0b0000, 0b1111, 0b1100, 0b1100, 0b1111, 0b1100, 0b1100, 0b1111),
                'F' to listOf(4, 0b0000, 0b1111, 0b1100, 0b1100, 0b1111, 0b1100, 0b1100, 0b1100),
                'G' to listOf(5, 0b00000, 0b01110, 0b11000, 0b11000, 0b11011, 0b11001, 0b11001, 0b01110),
                'H' to listOf(5, 0b00000, 0b11001, 0b11001, 0b11001, 0b11111, 0b11001, 0b11001, 0b11001),
                'I' to listOf(4, 0b0000, 0b1111, 0b0110, 0b0110, 0b0110, 0b0110, 0b0110, 0b1111),
                'J' to listOf(4, 0b0000, 0b1111, 0b0011, 0b0011, 0b0011, 0b0011, 0b1011, 0b0110),
                'K' to listOf(5, 0b00000, 0b11000, 0b11001, 0b11010, 0b11100, 0b11110, 0b11011, 0b11011),
                'L' to listOf(4, 0b0000, 0b1100, 0b1100, 0b1100, 0b1100, 0b1100, 0b1100, 0b1111),
                'M' to listOf(6, 0b000000, 0b111110, 0b110101, 0b110101, 0b110101, 0b110101, 0b110101, 0b110001),
                'N' to listOf(5, 0b00000, 0b10001, 0b11001, 0b11101, 0b11111, 0b11011, 0b11001, 0b11001),
                'O' to listOf(4, 0b0000, 0b0110, 0b1101, 0b1101, 0b1101, 0b1101, 0b1101, 0b0110),
                'P' to listOf(4, 0b0000, 0b1110, 0b1101, 0b1101, 0b1110, 0b1100, 0b1100, 0b1100),
                'Q' to listOf(5, 0b00000, 0b01100, 0b11010, 0b11010, 0b11010, 0b11010, 0b11011, 0b01101),
                'R' to listOf(4, 0b0000, 0b1110, 0b1101, 0b1101, 0b1110, 0b1101, 0b1101, 0b1101),
                'S' to listOf(4, 0b0000, 0b0110, 0b1101, 0b1100, 0b0110, 0b0001, 0b1101, 0b0110),
                'T' to listOf(4, 0b0000, 0b1111, 0b0110, 0b0110, 0b0110, 0b0110, 0b0110, 0b0110),
                'U' to listOf(4, 0b0000, 0b1101, 0b1101, 0b1101, 0b1101, 0b1101, 0b1101, 0b0110),
                'V' to listOf(5, 0b00000, 0b11001, 0b11001, 0b11001, 0b11001, 0b11001, 0b01110, 0b00100),
                'W' to listOf(6, 0b000000, 0b110001, 0b110101, 0b110101, 0b110101, 0b110101, 0b110101, 0b011010),
                'X' to listOf(5, 0b00000, 0b10001, 0b11011, 0b01110, 0b00100, 0b01110, 0b11011, 0b10001),
                'Y' to listOf(5, 0b00000, 0b11001, 0b11001, 0b11001, 0b01110, 0b00100, 0b00100, 0b00100),
                'Z' to listOf(4, 0b0000, 0b1111, 0b1111, 0b0001, 0b0010, 0b0100, 0b1001, 0b1111),
                '[' to listOf(3, 0b111, 0b100, 0b100, 0b100, 0b100, 0b100, 0b100, 0b100, 0b111),
                '\\' to listOf(7, 0b0000000, 0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001),
                ']' to listOf(3, 0b111, 0b001, 0b001, 0b001, 0b001, 0b001, 0b001, 0b001, 0b111),
                '^' to listOf(3, 0b010, 0b101),
                '_' to listOf(5, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b11111),
                '`' to listOf(3, 0b110, 0b010, 0b001),
                'a' to listOf(4, 0b0000, 0b0000, 0b0000, 0b0110, 0b0001, 0b1111, 0b1101, 0b0111),
                'b' to listOf(4, 0b0000, 0b1000, 0b1000, 0b1110, 0b1101, 0b1101, 0b1101, 0b0110),
                'c' to listOf(4, 0b0000, 0b0000, 0b0000, 0b0110, 0b1101, 0b1100, 0b1101, 0b0110),
                'd' to listOf(4, 0b0000, 0b0001, 0b0001, 0b0111, 0b1101, 0b1101, 0b1101, 0b0110),
                'e' to listOf(4, 0b0000, 0b0000, 0b0000, 0b0110, 0b1101, 0b1111, 0b1100, 0b0110),
                'f' to listOf(4, 0b0000, 0b0011, 0b0110, 0b1111, 0b0110, 0b0110, 0b0110, 0b1100),
                'g' to listOf(4, 0b0000, 0b0000, 0b0000, 0b0111, 0b1101, 0b1101, 0b0111, 0b0001, 0b1110),
                'h' to listOf(4, 0b0000, 0b0100, 0b1100, 0b1110, 0b1101, 0b1101, 0b1101, 0b1101),
                'i' to listOf(2, 0b00, 0b00, 0b11, 0b00, 0b11, 0b11, 0b11, 0b11),
                'j' to listOf(3, 0b000, 0b000, 0b011, 0b000, 0b011, 0b011, 0b011, 0b011, 0b110),
                'k' to listOf(4, 0b000, 0b0100, 0b1100, 0b1101, 0b1101, 0b1110, 0b1101, 0b1101),
                'l' to listOf(3, 0b000, 0b010, 0b110, 0b110, 0b110, 0b110, 0b110, 0b111),
                'm' to listOf(6, 0b000000, 0b000000, 0b000000, 0b011110, 0b110101, 0b110101, 0b110101, 0b110101),
                'n' to listOf(4, 0b0000, 0b0000, 0b0000, 0b1010, 0b1101, 0b1101, 0b1101, 0b1101),
                'o' to listOf(4, 0b0000, 0b0000, 0b0000, 0b0110, 0b1101, 0b1101, 0b1101, 0b0110),
                'p' to listOf(4, 0b0000, 0b0000, 0b0000, 0b0110, 0b1101, 0b1101, 0b1110, 0b1100, 0b1100),
                'q' to listOf(4, 0b0000, 0b0000, 0b0000, 0b0110, 0b1011, 0b1011, 0b1111, 0b0011, 0b0011),
                'r' to listOf(3, 0b000, 0b000, 0b000, 0b101, 0b110, 0b110, 0b110, 0b110),
                's' to listOf(4, 0b0000, 0b0000, 0b0000, 0b0111, 0b1100, 0b1111, 0b0001, 0b1110),
                't' to listOf(4, 0b0000, 0b0010, 0b0110, 0b1111, 0b0110, 0b0110, 0b0110, 0b0011),
                'u' to listOf(4, 0b0000, 0b0000, 0b0000, 0b1101, 0b1101, 0b1101, 0b1101, 0b0110),
                'v' to listOf(4, 0b0000, 0b0000, 0b0000, 0b1101, 0b1101, 0b1101, 0b0111, 0b0010),
                'w' to listOf(6, 0b000000, 0b000000, 0b000000, 0b110001, 0b110101, 0b110101, 0b110101, 0b011010),
                'x' to listOf(5, 0b00000, 0b00000, 0b00000, 0b11011, 0b01110, 0b00100, 0b01110, 0b11011),
                'y' to listOf(4, 0b0000, 0b0000, 0b0000, 0b1101, 0b1101, 0b1101, 0b1111, 0b0001, 0b0110),
                'z' to listOf(4, 0b0000, 0b0000, 0b0000, 0b1111, 0b1111, 0b0010, 0b0100, 0b1111),
                '{' to listOf(3, 0b000, 0b011, 0b010, 0b010, 0b100, 0b010, 0b010, 0b011),
                '|' to listOf(1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1),
                '}' to listOf(3, 0b000, 0b110, 0b010, 0b010, 0b001, 0b010, 0b010, 0b110),
                '~' to listOf(7, 0b0000000, 0b0000000, 0b0000000, 0b0110001, 0b1001110)
        )
    }

    override fun convert(message: String, color: Chemion.Color): Array<Array<Chemion.Color>> {
        val messageLength = message.length
        val grid = Array(computeWidth(message)) { Array(CHAR_HEIGHT) { Chemion.Color.OFF } }

        var curX = 0
        for (i in 0 until messageLength) {
            val charData = getChar(message[i])

            for (x in 0 until charData[0]) {
                for (y in 0 until charData.size - 1) {
                    val bit = charData[y + 1].getBit(charData[0] - 1 - x)
                    grid[curX][y] = if (bit == 0) Chemion.Color.OFF else color
                }
                curX++
            }
            curX++
        }
        return grid
    }

    private fun computeWidth(message: String) = message
            .toCharArray()
            .map { getChar(it)[0] + SPACE_BETWEEN_CHARS }
            .sum() - SPACE_BETWEEN_CHARS

    private fun getChar(char: Char) = CHARSETS.getOrElse(char) { CHARSETS.getValue('?') }

    private fun Int.getBit(position: Int) = (this shr position) and 1
}
